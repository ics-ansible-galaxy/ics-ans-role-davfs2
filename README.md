ics-ans-role-davfs2
===================

Ansible role to install davfs2, a file system driver to mount WebDAV resource.

Requirements
------------

- ansible >= 2.5
- molecule >= 2.6

Role Variables
--------------

```yaml
# Disable file locking by default
davfs2_use_locks: "0"
davfs2_mount: {}
davfs2_password: ''
```

To mount a specific webDAV resource, use the `davfs2_mount` variable:

```
davfs2_mount:
  user: username
  src: https://webdav.example.com/path/
  path: /home/username/webdav
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-davfs2
```

License
-------

BSD 2-clause
