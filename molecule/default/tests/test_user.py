import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ics-ans-role-davfs2-user')


def test_mount_unit_file_exists(host):
    assert host.file('/etc/systemd/system/home-operator-webdav.mount').exists


def test_mount_unit_file_enabled(host):
    assert host.service('home-operator-webdav.mount').is_enabled
